import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { DemoDial } from '../components/DemoDial'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <DemoDial
          width={800}
          demoVals={{
            a: .5,
            b: 1,
            c: 1,
            d: .4
          }}
          valuesStyle={{
            color: 'red',
            fontFamily: 'Roboto',
            fontWeight: '100'
          }}
        />
      </main>
    </div>
  )
}

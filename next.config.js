module.exports = {
    webpack: (config, {dev}) => {
        config.module.rules.push(
            {
                test: /\.(png|jpe?g|gif|obj|ico|svg)$/i,
                use: [
                    {
                    loader: 'file-loader',
                        options: {
                            // name: '[name]_[hash].[ext]',
                            publicPath: '/_next/static',
                            outputPath: 'static'
                        }
                    }
                    
                ]
            }
        )
        return config
    }
}
## Getting Started

---

You can import `DemoDial` component from `<project folder>/components/DemoDial`.

`./pages/index.js`:

```javascript
import { DemoDial } from '../components/DemoDial'
```


`DemoDial` takes one `required` and other `optional` props:


## `Required prop`
Required prop `demoVals` is used to set the `percentage` fill of the `semi-circles` in the `DemoDial`.

<br />

| Prop Name | Type  | Required |
| :---: | :---: | :---: |
| `demoVals` | object |Yes |

<br />

| Propterties  | Percentage Value Range | Required |
| :---: | :---: | :---: |
| `a` | `0 - 1` | Yes |
| `b` | `0 - 1` | Yes |
| `c` | `0 - 1` | Yes |
| `d` | `0 - 1` | Yes |

<br />
<br />

## `Other props`
The following props can be used to customize the `colors` of `semi-circles` filled part and `Major Ticks` in `DemoDial` and the `styles` of the `percentage values` using a prop `valuesStyle`.

<br />

| Prop Name  | Type | Color Code Type | Required |
| :---: | :---:   | :---: | :---: |
|`circleOneFilledColor` |string |  Hex Code | No |
|`circleTwoFilledColor` |string |  Hex Code | No |
|`circleThreeFilledColor` |string |  Hex Code | No |
|`circleFourFilledColor` |string |  Hex Code | No |
|`circleOneMajorTicksColor` |string |  Hex Code | No |
|`circleThreeMajorTicksColor` |string |  Hex Code | No |

<br />  

## `valuesStyle`
| Prop Name  | Type | Required |
| :---: | :---:   | :---: |
|`valuesStyle` |object | No |

<br />

The following prop can be used to change the `dimensions` of the `DemoDial` component. <em> Only `width` is needed,</em> `height` will be calculated based on the `design`.

<br />

| Prop  | Type | Required |
| :---: | :---: | :---: |
|`width` |Number | No |  

## `Basic Example`:

```javascript
    <DemoDial 
        demoVals={{ 
            a: .5,
            b: 1,
            c: 1,
            d: .4
          }}
         valuesStyle={{
            color: 'red',
            fontFamily: 'Roboto',
            fontWeight: '100'
          }}
    />
```

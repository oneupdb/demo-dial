import React, { useState, useEffect } from 'react'
import propTypes from 'prop-types'
import { TimelineMax, TweenMax } from 'gsap/dist/gsap'
import { GaugeDial } from './GaugeDial'
import { GaugeValue } from './GaugeValue'
import styles from '../../styles/DemoDial.module.css'
import { prototype } from 'file-loader'

export const DemoDial = ({ width, circleOneFilledColor, circleTwoFilledColor, circleThreeFilledColor, circleFourFilledColor, circleOneMajorTicksColor, circleThreeMajorTicksColor, demoVals, valuesStyle }) => {
  const [_width, setWidth] = useState(width)

  useEffect(() => {
    resizeDial()
    window.addEventListener('resize', handleResize)

    if (!demoVals) {
      throw new Error(`Add "demoVals" prop to "DemoDial" component as: demoVals: {
            a: 0 - 1,
            b: 0 - 1,
            c: 0 - 1,
            d: 0 - 1
        }`)
    }
    Object.keys(demoVals).map(k => revEngine(k, demoVals[k]))
  }, [])
  const resizeDial = () => {
    if (screen.width < _width) {
      setWidth(screen.width - 20)
    } else if (window.innerWidth < _width) {
      setWidth(window.innerWidth - 20)
    }
  }

  const handleResize = () => {
    if (screen.width < _width + 20) {
      setWidth(screen.width - 20)
    } else if (window.innerWidth < _width + 20) {
      setWidth(window.innerWidth - 20)
    } else {
        setWidth(width)
    }
  }
  const revEngine = (demoKey, demoVal) => {
    if (demoVal > 1) throw new Error(`demoVal for "${demoKey}" should be: 0 - 1`)

    const ele = `#filled-${demoKey}`
    const duration = 2
    const degree = 180 * Math.min(1, demoVal)

    const tl = new TimelineMax({
      onComplete: () => {
        TweenMax.fromTo(ele, 1, { rotation: degree, transformOrigin: 'top', ease: 'Sine.easeInOut' }, { rotation: (degree === 180 ? degree - 2 : degree - 2), transformOrigin: 'top', ease: 'Sine.easeInOut', yoyo: true, repeat: -1 })
      }
    })

    tl.set(ele, { transformOrigin: 'top' })
    tl.to(ele, { duration, rotation: degree, ease: 'Circ.easeOut()' })
  }

  return (
        <div className={styles.container}>

            <div className={styles.gaugeWrapper} style={{ width: _width, height: _width * 0.5024705882352941 }}>
                <GaugeDial
                    circleOneFilledColor={circleOneFilledColor}
                    circleTwoFilledColor={circleTwoFilledColor}
                    circleThreeFilledColor={circleThreeFilledColor}
                    circleFourFilledColor={circleFourFilledColor}
                    circleOneMajorTicksColor={circleOneMajorTicksColor}
                    circleThreeMajorTicksColor={circleThreeMajorTicksColor}
                />
            </div>
            <GaugeValue k={0} valuesStyle={valuesStyle} autoResizeBase={_width} value={demoVals.a * 100} />
            <GaugeValue k={1} valuesStyle={valuesStyle} autoResizeBase={_width} value={demoVals.b * 100} isMarginLeft />
            <GaugeValue k={2} valuesStyle={valuesStyle} autoResizeBase={_width} value={demoVals.c * 100} isMarginLeft />
            <GaugeValue k={3} valuesStyle={valuesStyle} autoResizeBase={_width} value={demoVals.d * 100} isMarginLeft />

        </div>
  )
}

DemoDial.defaultProps = {
  width: 680,
  circleOneFilledColor: '#460082',
  circleTwoFilledColor: '#44d7b6',
  circleThreeFilledColor: '#f22c7d',
  circleFourFilledColor: '#162fb3',
  circleOneMajorTicksColor: '#440080',
  circleThreeMajorTicksColor: '#e91e63'
}
DemoDial.propTypes = {
  demoVals: propTypes.shape({
    a: propTypes.number.isRequired,
    b: propTypes.number.isRequired,
    c: propTypes.number.isRequired,
    d: propTypes.number.isRequired
  }).isRequired,
  valuesStyle: propTypes.shape({
    color: propTypes.string,
    fontWeight: propTypes.oneOfType([
      propTypes.number,
      propTypes.string
    ]),
    fontFamily: propTypes.string
  })
}

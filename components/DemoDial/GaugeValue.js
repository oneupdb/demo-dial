import React from 'react'
import { TimelineMax } from 'gsap/dist/gsap'
import styles from '../../styles/DemoDial.module.css'

export const GaugeValue = ({ k, isMarginLeft, autoResizeBase, value, valuesStyle }) => {
  const [val, setVal] = React.useState(0)

  const fontSize = autoResizeBase * 0.01858
  const paddingTop = autoResizeBase * 0.0125
  const leftVals = [
    0,
    autoResizeBase * 0.04375,
    autoResizeBase * 0.09875,
    autoResizeBase * 0.1575
  ]

  const getStyles = () => (
    isMarginLeft
      ? ({
          left: leftVals[k],
          fontSize,
          paddingTop
        })
      : ({ fontSize, paddingTop })
  )
  const obj = { value: 0 }

  React.useEffect(() => {
    const tl = new TimelineMax()

    tl.to(obj, {
      duration: 2,
      value: value,
      ease: 'power4.easeIn()',
      onUpdate: () => {
        setVal(Math.round(obj.value))
      }
    })
  }, [])

  return (
        <div
            className={styles.filledValue}
            style={getStyles()}
        >
            <span id={`value-${k}`} className={styles.valueStyle + ' gaugeValues'} style={valuesStyle}>{val}%</span>
        </div>
  )
}

GaugeValue.defaultProps = {
  isMarginLeft: false
}

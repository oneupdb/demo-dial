import React from 'react'

export const GaugeDial = ({ circleOneFilledColor, circleTwoFilledColor, circleThreeFilledColor, circleFourFilledColor, circleOneMajorTicksColor, circleThreeMajorTicksColor }) => {
  return (
      <svg
        id="prefix__Layer_1"
        xmlns="http://www.w3.org/2000/svg"
        x={0}
        y={0}
        viewBox="0 0 1001 1005"
        xmlSpace="preserve"
      >
        <style>
          {
            `.prefix__st2,.prefix__st3{fill:#acacac;stroke:#fff;stroke-width:.25}.prefix__st3{fill:${circleThreeMajorTicksColor}}.prefix__st8{fill:#455a64;fill-opacity:2.000000e-03}.prefix__st9{fill:#8948c3}.prefix__st10{fill:${circleOneMajorTicksColor}}`
          }
        </style>
        <g id="prefix__semi-circle-4">
          <path
            id="prefix__empty_1_"
            d="M177.3 433.8c-4.8 22.6-7.2 45.6-7.2 68.7H164c0-120.2 64.1-231.3 168.2-291.4 20.4-11.8 41.9-21.3 64.3-28.6 114.3-37.1 239.8-10.5 329.1 70 70.9 63.7 111.4 154.6 111.4 250h-6.1c0-118-63-227.1-165.2-286.1-81.1-46.8-178.2-57-267.3-28.1-112.3 36.5-196.5 130.1-221.1 245.5z"
            opacity={0.4}
            fillOpacity={0.1}
          />
          <path
            id="filled-d"
            d="M823.7 571.2c4.8-22.6 7.2-45.6 7.2-68.7h6.1c0 120.2-64.1 231.3-168.2 291.4-20.4 11.8-41.9 21.3-64.3 28.6-114.3 37.1-239.8 10.5-329.1-70-70.9-63.7-111.4-154.6-111.4-250h6.1c0 118 63 227.1 165.2 286.1 81.1 46.8 178.2 57 267.3 28.1 112.3-36.5 196.5-130.1 221.1-245.5z"
            fill={circleFourFilledColor}
          />
        </g>
        <g id="prefix__semi-circle-3-wrapper" transform="translate(100 105)">
          <g id="prefix__pink-ticks" transform="translate(.286 .604)">
            <g id="prefix__Minor-tickmarks">
              <path
                id="prefix__Tick-minor-0"
                className="prefix__st2"
                d="M.5 394.1h9.8v2.7H.5v-2.7z"
              />
              <path
                id="prefix__Tick-minor-1"
                className="prefix__st2"
                d="M1.4 369l9.7.6c0 .4-.1.9-.1 1.3s-.1.9-.1 1.3l-9.7-.6c.1-.8.2-1.7.2-2.6z"
              />
              <path
                id="prefix__Tick-minor-2"
                className="prefix__st2"
                d="M3.9 344l9.7 1.3-.3 2.7-9.7-1.2c0-1 .2-1.9.3-2.8z"
              />
              <path
                id="prefix__Tick-minor-3"
                className="prefix__st2"
                d="M7.9 319.2l9.6 1.9c-.1.6-.2 1.2-.4 1.9 0 .3-.1.5-.1.8L7.4 322c.1-1 .3-1.9.5-2.8z"
              />
              <path
                id="prefix__Tick-minor-4"
                className="prefix__st2"
                d="M13.5 294.7l9.4 2.5c-.2.7-.4 1.4-.5 2.1l-9.6-1.9c.2-.9.4-1.8.7-2.7z"
              />
              <path
                id="prefix__Tick-minor-5"
                className="prefix__st2"
                d="M20.5 270.6l9.3 3c-.1.4-.3.8-.4 1.3s-.3.8-.4 1.3l-9.3-3 .8-2.6z"
              />
              <path
                id="prefix__Tick-minor-6"
                className="prefix__st2"
                d="M29.1 247l9.1 3.6c-.3.8-.7 1.7-1 2.5l-9.1-3.5c.4-.9.7-1.7 1-2.6z"
              />
              <path
                id="prefix__Tick-minor-7"
                className="prefix__st2"
                d="M39.2 224l8.8 4.2c-.3.6-.6 1.3-.9 1.9-.1.2-.2.3-.2.5l-8.8-4.1c.3-.9.7-1.7 1.1-2.5z"
              />
              <path
                id="prefix__Tick-minor-8"
                className="prefix__st2"
                d="M50.7 201.6l8.5 4.7c-.4.8-.9 1.6-1.3 2.3l-8.6-4.7c.5-.7.9-1.5 1.4-2.3z"
              />
              <path
                id="prefix__Tick-minor-9"
                className="prefix__st2"
                d="M63.5 180.1l8.2 5.3c-.4.6-.7 1.1-1.1 1.7-.1.2-.2.4-.4.6l-8.2-5.2c.5-.9 1-1.7 1.5-2.4z"
              />
              <path
                id="prefix__Tick-minor-10"
                className="prefix__st2"
                d="M77.7 159.3l7.9 5.8c-.4.5-.7 1-1.1 1.5-.2.2-.3.5-.5.7l-7.9-5.7c.4-.5.7-1 1.1-1.5.2-.3.4-.5.5-.8z"
              />
              <path
                id="prefix__Tick-minor-11"
                className="prefix__st2"
                d="M93.2 139.5l7.5 6.2-1.7 2.1-7.5-6.2c.5-.7 1.1-1.4 1.7-2.1z"
              />
              <path
                id="prefix__Tick-minor-12"
                className="prefix__st2"
                d="M109.9 120.7l7.1 6.7-.6.6c-.4.4-.8.9-1.3 1.3l-7.1-6.6c.6-.6 1.2-1.3 1.9-2z"
              />
              <path
                id="prefix__Tick-minor-13"
                className="prefix__st2"
                d="M127.7 103l6.6 7.1c-.4.4-.9.8-1.3 1.3l-.6.6-6.7-7.1c.7-.6 1.3-1.2 2-1.9z"
              />
              <path
                id="prefix__Tick-minor-14"
                className="prefix__st2"
                d="M146.6 86.5l6.2 7.5-2.1 1.7-6.2-7.5c.7-.5 1.4-1.1 2.1-1.7z"
              />
              <path
                id="prefix__Tick-minor-15"
                className="prefix__st2"
                d="M166.5 71.2l5.7 7.9c-.2.2-.5.3-.7.5-.5.4-1 .7-1.5 1.1l-5.8-7.9c.2-.2.5-.3.7-.5.6-.4 1.1-.8 1.6-1.1z"
              />
              <path
                id="prefix__Tick-minor-16"
                className="prefix__st2"
                d="M187.3 57.1l5.2 8.2c-.2.1-.4.2-.6.4-.6.4-1.1.7-1.7 1.1l-5.3-8.2c.9-.5 1.6-1 2.4-1.5z"
              />
              <path
                id="prefix__Tick-minor-17"
                className="prefix__st2"
                d="M209 44.4l4.7 8.6c-.8.4-1.6.9-2.3 1.3l-4.7-8.5c.7-.5 1.5-.9 2.3-1.4z"
              />
              <path
                id="prefix__Tick-minor-18"
                className="prefix__st2"
                d="M231.4 33.1l4.1 8.8c-.2.1-.3.2-.5.2-.6.3-1.3.6-1.9.9l-4.2-8.8c.8-.3 1.7-.7 2.5-1.1z"
              />
              <path
                id="prefix__Tick-minor-19"
                className="prefix__st2"
                d="M254.5 23.2l3.5 9.1c-.8.3-1.7.7-2.5 1l-3.6-9.1c.9-.3 1.7-.7 2.6-1z"
              />
              <path
                id="prefix__Tick-minor-20"
                className="prefix__st2"
                d="M278.2 14.8l3 9.3c-.4.1-.8.3-1.3.4s-.8.3-1.3.4l-3-9.3 2.6-.8z"
              />
              <path
                id="prefix__Tick-minor-21"
                className="prefix__st2"
                d="M302.3 7.8l2.4 9.4c-1.1.3-1.9.5-2.6.7l-2.5-9.4c.9-.2 1.8-.4 2.7-.7z"
              />
              <path
                id="prefix__Tick-minor-22"
                className="prefix__st2"
                d="M326.8 2.4l1.8 9.6c-.3 0-.5.1-.8.1-.6.1-1.2.2-1.9.4L324.1 3l2.7-.6z"
              />
              <path
                id="prefix__Tick-minor-23"
                className="prefix__st2"
                d="M351.7-1.4l1.2 9.7-2.7.3-1.3-9.7c.9-.1 1.9-.2 2.8-.3z"
              />
              <path
                id="prefix__Tick-minor-24"
                className="prefix__st2"
                d="M376.7-3.7l.6 9.7c-.4 0-.9.1-1.3.1s-.9.1-1.3.1l-.6-9.7c.8-.1 1.7-.1 2.6-.2z"
              />
              <path
                id="prefix__Tick-minor-25"
                className="prefix__st2"
                d="M401.8-4.4v9.8h-2.7v-9.8h2.7z"
              />
              <path
                id="prefix__Tick-minor-26"
                className="prefix__st2"
                d="M424.2-3.7c.9.1 1.8.1 2.7.2l-.6 9.7-2.7-.2.6-9.7z"
              />
              <path
                id="prefix__Tick-minor-27"
                className="prefix__st2"
                d="M449.2-1.4l2.7.3-1.3 9.7-2.7-.3 1.3-9.7z"
              />
              <path
                id="prefix__Tick-minor-28"
                className="prefix__st2"
                d="M474 2.4c.9.2 1.8.3 2.7.5l-1.9 9.6c-.6-.1-1.2-.2-1.9-.4-.3 0-.5-.1-.8-.1l1.9-9.6z"
              />
              <path
                id="prefix__Tick-minor-29"
                className="prefix__st2"
                d="M498.5 7.8c.9.2 1.8.4 2.7.7l-2.5 9.4c-.7-.2-1.4-.4-2.1-.5l1.9-9.6z"
              />
              <path
                id="prefix__Tick-minor-30"
                className="prefix__st2"
                d="M522.7 14.8l2.6.8-3 9.3c-.4-.1-.8-.3-1.3-.4-.4-.1-.8-.3-1.3-.4l3-9.3z"
              />
              <path
                id="prefix__Tick-minor-31"
                className="prefix__st2"
                d="M546.3 23.2c.9.3 1.7.7 2.6 1l-3.6 9.1c-.8-.3-1.7-.7-2.5-1l3.5-9.1z"
              />
              <path
                id="prefix__Tick-minor-32"
                className="prefix__st2"
                d="M569.4 33.1c.8.4 1.7.8 2.5 1.2l-4.2 8.8c-.6-.3-1.3-.6-1.9-.9-.2-.1-.3-.2-.5-.2l4.1-8.9z"
              />
              <path
                id="prefix__Tick-minor-33"
                className="prefix__st2"
                d="M591.9 44.4c.8.4 1.6.9 2.4 1.3l-4.7 8.5c-.8-.4-1.6-.9-2.3-1.3l4.6-8.5z"
              />
              <path
                id="prefix__Tick-minor-34"
                className="prefix__st2"
                d="M613.5 57.1c.8.5 1.6 1 2.3 1.5l-5.3 8.2c-.6-.4-1.1-.7-1.7-1.1-.2-.1-.4-.2-.6-.4l5.3-8.2z"
              />
              <path
                id="prefix__Tick-minor-35"
                className="prefix__st2"
                d="M634.4 71.2c.5.4 1 .7 1.5 1.1.2.2.5.3.7.5l-5.8 7.9c-.5-.4-1-.7-1.5-1.1-.2-.2-.5-.3-.7-.5l5.8-7.9z"
              />
              <path
                id="prefix__Tick-minor-36"
                className="prefix__st2"
                d="M654.2 86.5l2.1 1.8-6.2 7.5-2.1-1.7 6.2-7.6z"
              />
              <path
                id="prefix__Tick-minor-37"
                className="prefix__st2"
                d="M673.1 103c.7.6 1.3 1.2 2 1.9l-6.7 7.1-.6-.6c-.4-.4-.9-.8-1.3-1.3l6.6-7.1z"
              />
              <path
                id="prefix__Tick-minor-38"
                className="prefix__st2"
                d="M691 120.7c.6.7 1.3 1.3 1.9 2l-7.1 6.6c-.4-.4-.8-.9-1.3-1.3l-.6-.6 7.1-6.7z"
              />
              <path
                id="prefix__Tick-minor-39"
                className="prefix__st2"
                d="M707.7 139.5l1.8 2.1-7.5 6.2-1.7-2.1 7.4-6.2z"
              />
              <path
                id="prefix__Tick-minor-40"
                className="prefix__st2"
                d="M723.1 159.3c.2.2.3.5.5.7.4.5.7 1 1.1 1.5l-7.9 5.7c-.2-.2-.3-.5-.5-.7-.4-.5-.7-1-1.1-1.5l7.9-5.7z"
              />
              <path
                id="prefix__Tick-minor-41"
                className="prefix__st2"
                d="M737.3 180.1c.5.8 1 1.5 1.5 2.3l-8.2 5.2c-.1-.2-.2-.4-.4-.6-.4-.6-.7-1.1-1.1-1.7l8.2-5.2z"
              />
              <path
                id="prefix__Tick-minor-42"
                className="prefix__st2"
                d="M750.2 201.6c.4.8.9 1.6 1.3 2.4l-8.6 4.7c-.4-.8-.9-1.6-1.3-2.3l8.6-4.8z"
              />
              <path
                id="prefix__Tick-minor-43"
                className="prefix__st2"
                d="M761.7 224c.4.8.8 1.7 1.2 2.5l-8.8 4.1c-.1-.2-.2-.3-.2-.5-.3-.6-.6-1.3-.9-1.9l8.7-4.2z"
              />
              <path
                id="prefix__Tick-minor-44"
                className="prefix__st2"
                d="M771.7 247c.3.9.7 1.7 1 2.6l-9.1 3.5c-.3-.8-.7-1.7-1-2.5l9.1-3.6z"
              />
              <path
                id="prefix__Tick-minor-45"
                className="prefix__st2"
                d="M780.3 270.6l.8 2.6-9.3 3-.8-2.5 9.3-3.1z"
              />
              <path
                id="prefix__Tick-minor-46"
                className="prefix__st2"
                d="M787.4 294.7c.2.9.5 1.8.7 2.7l-9.4 2.4c-.3-1.1-.5-1.9-.7-2.6l9.4-2.5z"
              />
              <path
                id="prefix__Tick-minor-47"
                className="prefix__st2"
                d="M793 319.2c.2.9.3 1.8.5 2.7l-9.6 1.8c0-.3-.1-.5-.1-.8-.1-.6-.2-1.2-.4-1.9l9.6-1.8z"
              />
              <path
                id="prefix__Tick-minor-48"
                className="prefix__st2"
                d="M797 344l.3 2.7-9.7 1.2-.3-2.7 9.7-1.2z"
              />
              <path
                id="prefix__Tick-minor-49"
                className="prefix__st2"
                d="M799.4 369c.1.9.1 1.8.2 2.7l-9.7.6c0-.4-.1-.9-.1-1.3s-.1-.9-.1-1.3l9.7-.7z"
              />
            </g>
            <g id="prefix__Major-tickmarks">
              <path
                id="prefix__Tick-major-0"
                className="prefix__st3"
                d="M.6 392.1l19.7.2v6.4l-19.7.2c-.1-2.3-.1-4.5 0-6.8z"
              />
              <path
                id="prefix__Tick-major-5"
                className="prefix__st3"
                d="M21.2 268.7l18.7 6.3c-.7 2-1.3 4.1-2 6.1l-18.8-5.9c.7-2.2 1.4-4.4 2.1-6.5z"
              />
              <path
                id="prefix__Tick-major-10"
                className="prefix__st3"
                d="M78.9 157.7l15.9 11.8c-1.3 1.7-2.5 3.4-3.8 5.2l-16.1-11.5c1.3-1.8 2.7-3.7 4-5.5z"
              />
              <path
                id="prefix__Tick-major-15"
                className="prefix__st3"
                d="M168.1 70l11.5 16.1c-1.7 1.2-3.5 2.5-5.2 3.8L162.7 74c1.8-1.4 3.6-2.7 5.4-4z"
              />
              <path
                id="prefix__Tick-major-20"
                className="prefix__st3"
                d="M280.1 14.1L286 33c-2 .6-4.1 1.3-6.1 2l-6.3-18.7c2.2-.8 4.3-1.5 6.5-2.2z"
              />
              <path
                id="prefix__Tick-major-25"
                className="prefix__st3"
                d="M401.5-4.4h2.2l-.2 19.7h-6.4L397-4.4h6.7-2.2z"
              />
              <path
                id="prefix__Tick-major-30"
                className="prefix__st3"
                d="M520.8 14.1c2.1.7 4.3 1.4 6.4 2.1L520.9 35c-2-.7-4.1-1.3-6.1-2l6-18.9z"
              />
              <path
                id="prefix__Tick-major-35"
                className="prefix__st3"
                d="M632.7 70c1.8 1.3 3.7 2.6 5.5 4l-11.8 15.9c-1.7-1.3-3.4-2.5-5.2-3.8L632.7 70z"
              />
              <path
                id="prefix__Tick-major-40"
                className="prefix__st3"
                d="M721.9 157.7c1.3 1.8 2.7 3.6 4 5.5l-16.1 11.5c-1.2-1.7-2.5-3.5-3.8-5.2l15.9-11.8z"
              />
              <path
                id="prefix__Tick-major-45"
                className="prefix__st3"
                d="M779.7 268.7c.7 2.1 1.4 4.3 2.1 6.4L763 281c-.6-2-1.3-4.1-2-6.1l18.7-6.2z"
              />
              <path
                id="prefix__Tick-major-50"
                className="prefix__st3"
                d="M800.3 392.1v6.7l-19.7-.2v-6.4l19.7-.1z"
              />
            </g>
          </g>
          <g id="prefix__semi-circle-3">
            <path
              id="prefix__empty_2_"
              d="M668.1 101.8c84.2 75.9 132.3 182.7 132.3 296.1h-7.3c0-140.3-74.9-268.8-196.4-338.9C501.4 3.8 387.2-8.6 282.2 24.5l-3.2 1C145.6 68.9 45.4 180.2 16.3 317.4c-5.7 26.8-8.6 53-8.6 80.5H.4c0-142.9 76.2-273.8 200-345.2 24.2-14 49.8-25.4 76.4-34 135.9-44.2 285.1-12.5 391.3 83.1z"
              opacity={0.302}
              fillOpacity={0.1}
            />
            <path
              id="filled-c"
              d="M132.8 696C48.5 620.2.4 511 .4 397.6h7.3c0 140.3 74.9 271.1 196.4 341.3 95.4 55.1 209.6 67.5 314.5 34.4l3.2-1c133.4-43.4 233.6-154.6 262.8-291.9 5.7-26.8 8.6-55.4 8.6-82.8h7.3c0 142.9-76.2 276.1-200 347.6-24.2 14-49.8 25.4-76.4 34-136 44.2-285.1 12.5-391.3-83.2z"
              fill={circleThreeFilledColor}
            />
          </g>
        </g>
        <g
          id="prefix__semi-circle-2"
          transform="translate(45 52)"
          stroke="#fff"
          strokeWidth={0.25}
        >
          <path
            id="prefix__empty"
            d="M315 18c154.6-50.2 324.2-14.2 444.9 94.6 95.8 86.2 150.5 209 150.5 337.9h0-8c0-79.8-21.3-156.5-59.9-223.3-38.6-66.8-94.4-123.6-163.5-163.5C569.3.4 437.9-13.4 317.5 25.7 165.6 75 51.6 201.6 18.4 357.7c-6.5 30.5-9.8 61.6-9.8 92.8h0-8C.7 288 87.4 137.9 228.1 56.7 255.6 40.8 284.7 27.8 315 18z"
            opacity={0.355}
            fillOpacity={0.1}
          />
          <path
            id="filled-b"
            d="M596 883c-154.6 50.2-324.2 14.2-444.9-94.6C55.3 702.2.6 579.4.6 450.5h8c0 79.8 21.3 156.5 59.9 223.3 38.6 66.8 94.4 123.6 163.6 163.6 109.7 63.3 241.1 77.1 361.5 38 151.8-49.3 265.8-175.9 299-332.1 6.5-30.5 9.8-61.6 9.8-92.8h8c0 162.5-86.7 312.6-227.4 393.8-27.6 15.9-56.7 28.9-87 38.7z"
            fill={circleTwoFilledColor}
          />
        </g>
        <g id="prefix__semi-circle-1-wrapper">
          <g transform="translate(0 4)">
            <g id="prefix__Minor-tickmarks_1_" transform="translate(1 1)">
              <path
                id="prefix__Base-plate"
                className="prefix__st8"
                d="M497.6-1.9c178 0 342.4 94.9 431.4 249s89 344 0 498.1-253.4 249.1-431.4 249.1-342.4-94.9-431.4-249-89-344 0-498.1S319.7-1.9 497.6-1.9V8C323.2 8 162 101 74.8 252.1s-87.2 337.1 0 488.2 248.4 244.1 422.8 244.1 335.6-93 422.8-244.1 87.2-337.1 0-488.2S672 8 497.6 8v-9.9z"
              />
              <path
                id="prefix__Tick-minor-0_1_"
                className="prefix__st9"
                d="M-.5 497.7v-3h10v2.9l-10 .1z"
              />
              <path
                id="prefix__Tick-minor-1_1_"
                className="prefix__st9"
                d="M.4 466.4c.1-1 .1-2 .2-3l9.9.7c-.1 1-.1 1.9-.2 2.9l-9.9-.6z"
              />
              <path
                id="prefix__Tick-minor-2_1_"
                className="prefix__st9"
                d="M3.3 435.3c.1-1 .2-2 .4-3l9.9 1.3c-.1 1-.2 1.9-.4 2.9l-9.9-1.2z"
              />
              <path
                id="prefix__Tick-minor-3_1_"
                className="prefix__st9"
                d="M8.1 404.3c.2-1 .4-2 .6-2.9l9.8 1.9c-.2 1-.4 1.9-.5 2.9l-9.9-1.9z"
              />
              <path
                id="prefix__Tick-minor-4_1_"
                className="prefix__st9"
                d="M14.8 373.8c.2-1 .5-1.9.7-2.9l9.6 2.5c-.2.9-.5 1.9-.7 2.8l-9.6-2.4z"
              />
              <path
                id="prefix__Tick-minor-5_1_"
                className="prefix__st9"
                d="M23.4 343.7c.3-.9.6-1.9.9-2.8l9.5 3.1c-.3.9-.6 1.9-.9 2.8l-9.5-3.1z"
              />
              <path
                id="prefix__Tick-minor-6_1_"
                className="prefix__st9"
                d="M33.9 314.2c.4-.9.7-1.9 1.1-2.8l9.2 3.7c-.4.9-.7 1.8-1.1 2.7l-9.2-3.6z"
              />
              <path
                id="prefix__Tick-minor-7_1_"
                className="prefix__st9"
                d="M46.3 285.5c.4-.9.8-1.8 1.3-2.7l9 4.3c-.4.9-.8 1.8-1.2 2.6l-9.1-4.2z"
              />
              <path
                id="prefix__Tick-minor-8_1_"
                className="prefix__st9"
                d="M60.4 257.5c.5-.9 1-1.8 1.4-2.6l8.7 4.8c-.5.9-.9 1.7-1.4 2.6l-8.7-4.8z"
              />
              <path
                id="prefix__Tick-minor-9_1_"
                className="prefix__st9"
                d="M76.2 230.6c.5-.8 1.1-1.7 1.6-2.5l8.4 5.4c-.5.8-1.1 1.6-1.6 2.5l-8.4-5.4z"
              />
              <path
                id="prefix__Tick-minor-10_1_"
                className="prefix__st9"
                d="M93.8 204.6l1.8-2.4 8 5.9c-.6.8-1.2 1.6-1.7 2.4l-8.1-5.9z"
              />
              <path
                id="prefix__Tick-minor-11_1_"
                className="prefix__st9"
                d="M112.9 179.8c.6-.8 1.3-1.5 1.9-2.3l7.7 6.4c-.6.7-1.3 1.5-1.9 2.3l-7.7-6.4z"
              />
              <path
                id="prefix__Tick-minor-12_1_"
                className="prefix__st9"
                d="M133.5 156.3c.7-.7 1.4-1.5 2-2.2l7.2 6.8c-.7.7-1.3 1.4-2 2.1l-7.2-6.7z"
              />
              <path
                id="prefix__Tick-minor-13_1_"
                className="prefix__st9"
                d="M155.5 134.1c.7-.7 1.5-1.4 2.2-2l6.8 7.3c-.7.7-1.4 1.3-2.1 2l-6.9-7.3z"
              />
              <path
                id="prefix__Tick-minor-14_1_"
                className="prefix__st9"
                d="M179 113.3c.8-.6 1.5-1.3 2.3-1.9l6.3 7.7c-.8.6-1.5 1.2-2.3 1.9l-6.3-7.7z"
              />
              <path
                id="prefix__Tick-minor-15_1_"
                className="prefix__st9"
                d="M203.6 94.1l2.4-1.8 5.8 8.1c-.8.6-1.6 1.1-2.4 1.7l-5.8-8z"
              />
              <path
                id="prefix__Tick-minor-16_1_"
                className="prefix__st9"
                d="M229.5 76.4c.8-.5 1.7-1.1 2.5-1.6l5.3 8.4c-.8.5-1.6 1-2.5 1.6l-5.3-8.4z"
              />
              <path
                id="prefix__Tick-minor-17_1_"
                className="prefix__st9"
                d="M256.3 60.4c.9-.5 1.7-1 2.6-1.4l4.8 8.7c-.9.5-1.7.9-2.6 1.4l-4.8-8.7z"
              />
              <path
                id="prefix__Tick-minor-18_1_"
                className="prefix__st9"
                d="M284.2 46.1c.9-.4 1.8-.9 2.7-1.3l4.2 9c-.9.4-1.8.8-2.6 1.2l-4.3-8.9z"
              />
              <path
                id="prefix__Tick-minor-19_1_"
                className="prefix__st9"
                d="M312.9 33.6c.9-.4 1.9-.7 2.8-1.1l3.6 9.3c-.9.4-1.8.7-2.7 1.1l-3.7-9.3z"
              />
              <path
                id="prefix__Tick-minor-20_1_"
                className="prefix__st9"
                d="M342.3 22.9c.9-.3 1.9-.6 2.8-.9l3 9.5c-.9.3-1.9.6-2.8.9l-3-9.5z"
              />
              <path
                id="prefix__Tick-minor-21_1_"
                className="prefix__st9"
                d="M372.3 14.1c1-.2 1.9-.5 2.9-.7l2.5 9.7c-.9.2-1.9.5-2.8.7l-2.6-9.7z"
              />
              <path
                id="prefix__Tick-minor-22_1_"
                className="prefix__st9"
                d="M402.8 7.2c1-.2 2-.4 2.9-.6l1.8 9.8c-1 .2-1.9.4-2.9.5l-1.8-9.7z"
              />
              <path
                id="prefix__Tick-minor-23_1_"
                className="prefix__st9"
                d="M433.7 2.2c1-.1 2-.2 3-.4l1.2 9.9c-1 .1-1.9.2-2.9.4l-1.3-9.9z"
              />
              <path
                id="prefix__Tick-minor-24_1_"
                className="prefix__st9"
                d="M464.9-.8c1-.1 2-.1 3-.2l.6 10c-1 .1-1.9.1-2.9.2l-.7-10z"
              />
              <path
                id="prefix__Tick-minor-25_1_"
                className="prefix__st9"
                d="M496.1-1.9h3v10h-2.9l-.1-10z"
              />
              <path
                id="prefix__Tick-minor-26_1_"
                className="prefix__st9"
                d="M527.4-1c1 .1 2 .1 3 .2l-.7 9.9c-1-.1-1.9-.1-2.9-.2l.6-9.9z"
              />
              <path
                id="prefix__Tick-minor-27_1_"
                className="prefix__st9"
                d="M558.6 1.8c1 .1 2 .2 3 .4l-1.3 9.9c-1-.1-1.9-.2-2.9-.4l1.2-9.9z"
              />
              <path
                id="prefix__Tick-minor-28_1_"
                className="prefix__st9"
                d="M589.5 6.6c1 .2 2 .4 2.9.6l-1.9 9.8c-1-.2-1.9-.4-2.9-.5l1.9-9.9z"
              />
              <path
                id="prefix__Tick-minor-29_1_"
                className="prefix__st9"
                d="M620 13.4c1 .2 1.9.5 2.9.7l-2.5 9.6c-.9-.2-1.9-.5-2.8-.7l2.4-9.6z"
              />
              <path
                id="prefix__Tick-minor-30_1_"
                className="prefix__st9"
                d="M650.1 22c.9.3 1.9.6 2.8.9l-3.1 9.5c-.9-.3-1.9-.6-2.8-.9l3.1-9.5z"
              />
              <path
                id="prefix__Tick-minor-31_1_"
                className="prefix__st9"
                d="M679.6 32.5c.9.4 1.9.7 2.8 1.1l-3.7 9.2c-.9-.4-1.8-.7-2.7-1.1l3.6-9.2z"
              />
              <path
                id="prefix__Tick-minor-32_1_"
                className="prefix__st9"
                d="M708.4 44.9c.9.4 1.8.8 2.7 1.3l-4.3 9-2.7-1.2 4.3-9.1z"
              />
              <path
                id="prefix__Tick-minor-33_1_"
                className="prefix__st9"
                d="M736.3 59c.9.5 1.8 1 2.6 1.4l-4.8 8.7c-.9-.5-1.7-.9-2.6-1.4l4.8-8.7z"
              />
              <path
                id="prefix__Tick-minor-34_1_"
                className="prefix__st9"
                d="M763.3 74.8c.8.5 1.7 1.1 2.5 1.6l-5.4 8.4c-.8-.5-1.6-1.1-2.5-1.6l5.4-8.4z"
              />
              <path
                id="prefix__Tick-minor-35_1_"
                className="prefix__st9"
                d="M789.2 92.3l2.4 1.8-5.9 8c-.8-.6-1.6-1.2-2.4-1.7l5.9-8.1z"
              />
              <path
                id="prefix__Tick-minor-36_1_"
                className="prefix__st9"
                d="M814 111.4c.8.6 1.5 1.3 2.3 1.9l-6.4 7.7c-.7-.6-1.5-1.3-2.3-1.9l6.4-7.7z"
              />
              <path
                id="prefix__Tick-minor-37_1_"
                className="prefix__st9"
                d="M837.5 132.1c.7.7 1.5 1.4 2.2 2l-6.8 7.2c-.7-.7-1.4-1.3-2.1-2l6.7-7.2z"
              />
              <path
                id="prefix__Tick-minor-38_1_"
                className="prefix__st9"
                d="M859.7 154.1c.7.7 1.4 1.5 2 2.2l-7.3 6.8c-.7-.7-1.3-1.4-2-2.1l7.3-6.9z"
              />
              <path
                id="prefix__Tick-minor-39_1_"
                className="prefix__st9"
                d="M880.5 177.5c.6.8 1.3 1.5 1.9 2.3l-7.7 6.3c-.6-.8-1.2-1.5-1.9-2.3l7.7-6.3z"
              />
              <path
                id="prefix__Tick-minor-40_1_"
                className="prefix__st9"
                d="M899.7 202.2l1.8 2.4-8.1 5.8c-.6-.8-1.1-1.6-1.7-2.4l8-5.8z"
              />
              <path
                id="prefix__Tick-minor-41_1_"
                className="prefix__st9"
                d="M917.4 228c.5.8 1.1 1.7 1.6 2.5l-8.4 5.3c-.5-.8-1-1.6-1.6-2.5l8.4-5.3z"
              />
              <path
                id="prefix__Tick-minor-42_1_"
                className="prefix__st9"
                d="M933.4 254.9c.5.9 1 1.7 1.4 2.6l-8.7 4.8c-.5-.9-.9-1.7-1.4-2.6l8.7-4.8z"
              />
              <path
                id="prefix__Tick-minor-43_1_"
                className="prefix__st9"
                d="M947.7 282.8c.4.9.9 1.8 1.3 2.7l-9 4.2c-.4-.9-.8-1.8-1.2-2.6l8.9-4.3z"
              />
              <path
                id="prefix__Tick-minor-44_1_"
                className="prefix__st9"
                d="M960.2 311.4c.4.9.7 1.9 1.1 2.8l-9.3 3.6c-.4-.9-.7-1.8-1.1-2.7l9.3-3.7z"
              />
              <path
                id="prefix__Tick-minor-45_1_"
                className="prefix__st9"
                d="M970.9 340.9c.3.9.6 1.9.9 2.8l-9.5 3c-.3-.9-.6-1.9-.9-2.8l9.5-3z"
              />
              <path
                id="prefix__Tick-minor-46_1_"
                className="prefix__st9"
                d="M979.7 370.9c.2 1 .5 1.9.7 2.9l-9.7 2.5c-.2-.9-.5-1.9-.7-2.8l9.7-2.6z"
              />
              <path
                id="prefix__Tick-minor-47_1_"
                className="prefix__st9"
                d="M986.6 401.4c.2 1 .4 2 .6 2.9l-9.8 1.8c-.2-1-.4-1.9-.5-2.9l9.7-1.8z"
              />
              <path
                id="prefix__Tick-minor-48_1_"
                className="prefix__st9"
                d="M991.6 432.3c.1 1 .2 2 .4 3l-9.9 1.2c-.1-1-.2-1.9-.4-2.9l9.9-1.3z"
              />
              <path
                id="prefix__Tick-minor-49_1_"
                className="prefix__st9"
                d="M994.7 463.4c.1 1 .1 2 .2 3l-10 .6c-.1-1-.1-1.9-.2-2.9l10-.7z"
              />
            </g>
            <g id="prefix__Major-tickmarks_1_">
              <path
                id="prefix__Base-plate_1_"
                className="prefix__st8"
                d="M498.6-.9C676.6-.9 841 94 930 248.1s89 344 0 498.1-253.4 249.1-431.4 249.1-342.4-94.9-431.4-249-89-344 0-498.1S320.7-.9 498.6-.9V19c-170.8 0-328.7 91.1-414.1 239.1s-85.4 330.2 0 478.2 243.3 239.1 414.1 239.1 328.7-91.1 414.1-239.1 85.4-330.2 0-478.2S669.5 19 498.6 19V-.9z"
              />
              <path
                id="prefix__Tick-major-0_1_"
                className="prefix__st10"
                d="M.5 500.7v-7l19.9.1v6.7l-19.9.2z"
              />
              <path
                id="prefix__Tick-major-5_1_"
                className="prefix__st10"
                d="M23.8 346.6c.7-2.2 1.4-4.4 2.2-6.6l18.9 6.3c-.7 2.1-1.4 4.2-2.1 6.4l-19-6.1z"
              />
              <path
                id="prefix__Tick-major-10_1_"
                className="prefix__st10"
                d="M93.6 207.2c1.4-1.9 2.7-3.8 4.1-5.6l16 11.8-3.9 5.4-16.2-11.6z"
              />
              <path
                id="prefix__Tick-major-15_1_"
                className="prefix__st10"
                d="M203 96.3c1.9-1.4 3.8-2.7 5.6-4.1l11.6 16.2-5.4 3.9-11.8-16z"
              />
              <path
                id="prefix__Tick-major-20_1_"
                className="prefix__st10"
                d="M341.4 24.5c2.2-.7 4.4-1.4 6.6-2.2l6 19c-2.1.7-4.2 1.4-6.4 2.1l-6.2-18.9z"
              />
              <path
                id="prefix__Tick-major-25_1_"
                className="prefix__st10"
                d="M495.1-.9h7L502 19h-6.7l-.2-19.9z"
              />
              <path
                id="prefix__Tick-major-30_1_"
                className="prefix__st10"
                d="M649.2 22.4c2.2.7 4.4 1.4 6.6 2.2l-6.3 18.9c-2.1-.7-4.2-1.4-6.4-2.1l6.1-19z"
              />
              <path
                id="prefix__Tick-major-35_1_"
                className="prefix__st10"
                d="M788.6 92.2c1.9 1.4 3.8 2.7 5.6 4.1l-11.8 16-5.4-3.9 11.6-16.2z"
              />
              <path
                id="prefix__Tick-major-40_1_"
                className="prefix__st10"
                d="M899.5 201.6c1.4 1.9 2.7 3.8 4.1 5.6l-16.2 11.6-3.9-5.4 16-11.8z"
              />
              <path
                id="prefix__Tick-major-45_1_"
                className="prefix__st10"
                d="M971.3 340c.7 2.2 1.4 4.4 2.2 6.6l-19 6c-.7-2.1-1.4-4.2-2.1-6.4l18.9-6.2z"
              />
              <path
                id="prefix__Tick-major-50_1_"
                className="prefix__st10"
                d="M996.7 493.7v7l-19.9-.1v-6.7l19.9-.2z"
              />
            </g>
          </g>
          <g id="prefix__semi-circle-1" transform="translate(1)">
            <path
              id="prefix__empty_3_"
              d="M19.3 400.6c-7.1 33.6-10.7 67.9-10.7 102.3H-.5c0-179 95.3-344.4 250-433.9 30.3-17.5 62.3-31.8 95.5-42.6 169.9-55.3 356.3-15.6 489.1 104.2 105.3 95 165.4 230.3 165.4 372.3h-9.1c0-175.7-93.6-338.1-245.5-426C624.5 7.2 480.1-8 347.8 35.1 181 89.4 55.8 228.7 19.3 400.6z"
              opacity={0.297}
              fillOpacity={0.1}
            />
            <path
              id="filled-a"
              d="M979.5 605c7.2-33.6 10.8-67.9 10.9-102.3h9.1c-.2 179-95.8 344.2-250.6 433.5-30.3 17.5-62.3 31.7-95.5 42.5-170 55.1-356.3 15.1-488.9-104.8C59.3 778.8-.7 644.7-.5 502.7h9.1c-.2 175.7 93.1 337 244.9 425.1a489.92 489.92 0 00397.1 42.4C817.4 916.1 942.8 776.9 979.5 605z"
              fill={circleOneFilledColor}
            />
          </g>
        </g>
      </svg>
  )
}
